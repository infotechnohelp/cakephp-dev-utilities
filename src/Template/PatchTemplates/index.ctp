<style>

    .template-container {
        text-align: right;
        background-color: lightgoldenrodyellow;
        padding: 0 10px 10px 10px;
        margin: 10px;
    }

    .wide-input {
        width: 100%;
    }

    .float-right {
        float: right;
    }

    .float-left {
        float: left;
    }

    button {
        cursor: pointer;
    }

    pre {
        display: inline-block;
        padding-right: 15px;
    }

    .pre-container {
        margin: 10px;
    }

    .pre-container > button {
        vertical-align: top;
    }

    .fixed {
        position: fixed;
        width: 100%;
        margin: 0;
        padding: 15px;
        box-sizing: border-box;
        left: 0;
        top: 0;
    }

    .white {
        background-color: white;
    }

    .links, .selections, h2 {
        text-align: right;
        margin: 10px;
    }

    input[name="pluginTableAlias"]{
        width:300px;
    }

    input[name="entityFields"]{
        width:100px;
    }
</style>


<div id="file-path-container" class="fixed white">
    <input id="file-path" class="wide-input" placeholder="path/to/file"
    value="<?= $requestData['filePath'] ?? null ?>">

    <button class="paste">Paste</button>

    <button class="paste float-right">Paste</button>
</div>

<div class="links">
    <?php foreach ($data as $templateGroup => $templates) { ?>

        <a href="#template-group-<?= $templateGroup ?>"><?= $templateGroup ?></a>

    <?php } ?>
</div>

<div class="selections">
    <span>pluginTableAlias</span>
    <select class="select-plugin-table-alias">
        <option value="0">Choose</option>

        <?php foreach ($pluginTableAliases as $tableTitle) { ?>
            <option value="<?= $tableTitle ?>"><?= $tableTitle ?></option>
        <?php } ?>

    </select>

    <span>tableTitle</span>
    <select class="select-table-title">
        <option value="0">Choose</option>

        <?php foreach ($tables as $tableTitle) { ?>
            <option value="<?= $tableTitle ?>"><?= $tableTitle ?></option>
        <?php } ?>

    </select>
</div>

<?php foreach ($data as $templateGroup => $templates) { ?>

    <div class="template-group-container" id="template-group-<?= $templateGroup ?>">

        <h2><?= $templateGroup ?></h2>

        <?php foreach ($templates as $template) { ?>

            <div class="template-container">

                <h4 id="<?= $template['class'] ?>"><?= $template['class'] ?></h4>

                <button class="patch">Patch</button>

                <button class="patch float-left">Patch</button>

                <?php foreach ($template['input'] as $key => $options) { ?>

                    <span><strong><?= $key ?></strong>(<?= $options[0] ?>)</span>
                    <input name="<?= $key ?>" placeholder="<?= $options[1] ?>">

                <?php } ?>

                <button class="patch">Patch</button>

                <?php $i = 0;
                foreach ($template['patches'] as $patch) { ?>

                    <div class="pre-container">

                        <pre><?= $patch ?></pre>

                        <button class="ignore" data-ignored="0" data-id="<?= $i ?>">Ignore</button>

                    </div>

                    <hr>

                    <?php $i++;
                } ?>

                <button class="patch">Patch</button>

                <button class="patch float-left">Patch</button>

            </div>

            <div class="links">
                <?php foreach ($data as $templateGroup => $templates) { ?>

                    <a href="#template-group-<?= $templateGroup ?>"><?= $templateGroup ?></a>

                <?php } ?>
            </div>

            <div class="selections">
                <span>pluginTableAlias</span>
                <select class="select-plugin-table-alias">
                    <option value="0">Choose</option>

                    <?php foreach ($pluginTableAliases as $tableTitle) { ?>
                        <option value="<?= $tableTitle ?>"><?= $tableTitle ?></option>
                    <?php } ?>

                </select>

                <span>tableTitle</span>
                <select class="select-table-title">
                    <option value="0">Choose</option>

                    <?php foreach ($tables as $tableTitle) { ?>
                        <option value="<?= $tableTitle ?>"><?= $tableTitle ?></option>
                    <?php } ?>

                </select>
            </div>
            
        <?php } ?>

    </div>

<?php } ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

    $.extend(
        {
            redirectPost: function (location, args) {
                var form = '';
                $.each(args, function (key, value) {
                    value = value.split('"').join('\"')
                    form += "<input type='hidden' name='" + key + "' value='" + value + "'>";
                });

                $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
            }
        });

    $(function () {

        $('body, html').css('padding-top', $('#file-path-container').height());

        $('.select-table-title').change(function(){
            var tableTitle = $(this).val();

            $('input[name="tableTitle"]').val(tableTitle);
            $('.select-table-title').val(tableTitle);
        });

        $('.select-plugin-table-alias').change(function(){
            var tableTitle = $(this).val();

            $('input[name="pluginTableAlias"]').val(tableTitle);
            $('.select-plugin-table-alias').val(tableTitle);
        });

        $('.paste').click(function () {
            navigator.clipboard.readText().then(text => {
                 $('#file-path').val(text);
            });
        });

        $('.ignore').click(function () {
            var ignored = $(this).attr('data-ignored');

            if (parseInt(ignored) === 1) {
                $(this).text('Ignore').attr('data-ignored', 0).siblings('pre').css('opacity', 1);
                return;
            }

            $(this).text('Enable').attr('data-ignored', 1).siblings('pre').css('opacity', '.1');
        });

        $('.patch').click(function () {

            var filePath = $('#file-path').val();

            var templateClass = $(this).siblings('h4').text();

            var input = [];

            $(this).parents('.template-container').find('input').each(function () {
                input.push($(this).val());
            });

            var ignoredPatches = [];

            var i = 0;

            $(this).siblings('.pre-container').each(function () {
                var ignored = $(this).find('.ignore').attr('data-ignored');

                if (parseInt(ignored) === 1) {
                    ignoredPatches.push(i);
                }

                i++;
            });

            $.redirectPost('patch-templates/patch', {
                filePath: filePath,
                templateClass: templateClass,
                inputJson: JSON.stringify(input),
                ignoredPatches: JSON.stringify(ignoredPatches)
            });
        });
    });

</script>
