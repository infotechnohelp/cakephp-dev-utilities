<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class Template
{
    protected $inputKeys = [];

    protected $patches = [];

    protected function __construct(array $input = null)
    {
        $this->validateInput($input);
    }

    protected function prepareInput(array $input = null): array
    {
        $demo = ($input === null) ? true : false;

        $result = [];

        $i = 0;

        foreach ($this->inputKeys as $key => $options) {
            list($type, $placeholder) = $options;

            $result[$key] = ($demo) ? $placeholder : $input[$i++];
        }

        return $result;
    }


    public function getPatches(): array
    {
        return $this->patches;
    }

    public function getPatchAmount(): int
    {
        return count($this->patches);
    }

    public function getInputKeys()
    {
        return $this->inputKeys;
    }

    // @todo
    public function validateInput(): bool
    {
        return true;
    }

    protected function getEntityFields(string $tableTitle, array $ignoredFields = ['id', 'created', 'modified']): array
    {
        /** @var Table $Table */
        $Table = TableRegistry::getTableLocator()->get($tableTitle);
        
        $columns = $Table->schema()->columns();

        $result = [];

        foreach ($columns as $column) {
            if (!in_array($column, $ignoredFields, true)) {
                $result[] = $column;
            }
        }

        return $result;
    }
}