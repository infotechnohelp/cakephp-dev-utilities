<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\Table;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;

/**
 * Class UpdateEntityById
 * @package Infotechnohelp\CakeDevUtilities\PatchTemplates\Table
 */
class UpdateEntityById extends Template
{
    /**
     * @var array
     */
    protected $inputKeys = [
        'tableTitle' => ['string', 'Articles'],
        'entityFields' => ['automatic', 'Leave empty']
    ];

    /**
     * UpdateEntityById constructor.
     * @param array|null $input
     */
    public function __construct(array $input = null)
    {
        parent::__construct($input);

        $preparedInput = $this->prepareInput($input);

        $this->patches = [
            $this->usages($preparedInput),
            $this->template($preparedInput),
        ];
    }

    /**
     * @param array $_
     * @return string
     */
    private function usages(array $_): string
    {
        $tableTitle = $_['tableTitle'];

        $entityTitle = Inflector::singularize($_['tableTitle']);

        return "use Cake\ORM\TableRegistry;\n" .
            "use App\Model\Table\\{$tableTitle}Table;\n" .
            "use App\Model\Entity\\$entityTitle;\n";
    }

    /**
     * @param array $_
     * @return string
     */
    private function template(array $_): string
    {
        $tableTitle = $_['tableTitle'];

        $entityTitle = Inflector::singularize($_['tableTitle']);

        $fields = ['field1', 'field2'];

        if(TableRegistry::getTableLocator()->get($_['tableTitle'])->getEntityClass() !== 'Cake\ORM\Entity'){
            $fields = $this->getEntityFields($_['tableTitle']);
        }

        $template =
            "/** @var {$tableTitle}Table \${$tableTitle}Table */\n" .
            "\${$tableTitle}Table = TableRegistry::getTableLocator()->get('$tableTitle');" .
            "\n\n" .
            "/** @var $entityTitle \$$entityTitle */\n" .
            "\$$entityTitle = \${$tableTitle}Table->get(\$id);\n\n";

        foreach ($fields as $field) {
            $template .= "\${$entityTitle}->set('$field', '');\n\n";
        }

        $template .=
            "\$validationErrors = \${$tableTitle}Table->getValidator()->errors(\${$entityTitle}->toArray(), false);\n\n".
            "if (empty(\$validationErrors)) {\n" .
            "\${$tableTitle}Table->saveOrFail(\$$entityTitle);\n" .
            "}";

        return $template;
    }
}