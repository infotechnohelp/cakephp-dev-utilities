<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\Table;

use App\Model\Entity\Note;
use Cake\ORM\ResultSet;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;

/**
 * Class FetchAllRows
 * @package Infotechnohelp\CakeDevUtilities\PatchTemplates\Table
 */
class FetchAllRows extends Template
{
    /**
     * @var array
     */
    protected $inputKeys = [
        'tableTitle' => ['string', 'Articles'],
    ];

    /**
     * FetchAllRows constructor.
     * @param array|null $input
     */
    public function __construct(array $input = null)
    {
        parent::__construct($input);

        $preparedInput = $this->prepareInput($input);

        $this->patches = [
            $this->usages($preparedInput),
            $this->template($preparedInput),
        ];
    }

    /**
     * @param array $_
     * @return string
     */
    private function usages(array $_): string
    {
        $tableTitle = $_['tableTitle'];

        $entityTitle = Inflector::singularize($_['tableTitle']);

        return "use Cake\ORM\TableRegistry;\n" .
            "use Cake\ORM\ResultSet;\n" .
            "use App\Model\Table\\{$tableTitle}Table;\n" .
            "use App\Model\Entity\\$entityTitle;\n";
    }

    /**
     * @param array $_
     * @return string
     */
    private static function template(array $_): string
    {
        $tableTitle = $_['tableTitle'];

        $entityTitle = Inflector::singularize($_['tableTitle']);

        $template =
            "/** @var {$tableTitle}Table \${$tableTitle}Table */\n" .
            "\${$tableTitle}Table = TableRegistry::getTableLocator()->get('$tableTitle');" .
            "\n\n" .
            " /** @var ResultSet \${$tableTitle} */\n" .
            "\${$tableTitle} = \${$tableTitle}Table->find()->all();";

        return $template;
    }
}