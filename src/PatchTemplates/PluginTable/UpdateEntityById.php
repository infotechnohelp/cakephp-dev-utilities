<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\PluginTable;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;

/**
 * Class UpdateEntityById
 * @package Infotechnohelp\CakeDevUtilities\PatchTemplates\PluginTable
 */
class UpdateEntityById extends Template
{
    /**
     * @var array
     */
    protected $inputKeys = [
        'pluginTableAlias' => ['string', 'Infotechnohelp/CakeDevUtilities.TestEntities'],
        'entityFields' => ['automatic', 'Leave empty']
    ];

    /**
     * UpdateEntityById constructor.
     * @param array|null $input
     */
    public function __construct(array $input = null)
    {
        parent::__construct($input);

        $preparedInput = $this->prepareInput($input);

        $this->patches = [
            $this->usages($preparedInput),
            $this->template($preparedInput),
        ];
    }

    /**
     * @param array $_
     * @return string
     */
    private function usages(array $_): string
    {
        $pluginTableAlias = $_['pluginTableAlias'];

        $tableTitle = explode('.', $pluginTableAlias)[1];

        $Table = TableRegistry::getTableLocator()->get($pluginTableAlias);

        $entityClass = $Table->getEntityClass();

        $tableClass = explode('Entity', $entityClass)[0] . "Table\\{$tableTitle}Table" ;

        return "use Cake\ORM\TableRegistry;\n" .
            "use $tableClass;\n" .
            "use $entityClass;\n";
    }

    /**
     * @param array $_
     * @return string
     */
    private function template(array $_): string
    {
        $pluginTableAlias = $_['pluginTableAlias'];

        $tableTitle = explode('.', $pluginTableAlias)[1];

        $Table = TableRegistry::getTableLocator()->get($pluginTableAlias);

        $entityClass = $Table->getEntityClass();

        $tableClass = explode('Entity', $entityClass)[0] . "Table\\{$tableTitle}Table" ;

        $fields = $this->getEntityFields($pluginTableAlias);

        $entityTitle = Inflector::singularize($tableTitle);

        $template =
            "/** @var {$tableTitle}Table \${$tableTitle}Table */\n" .
            "\${$tableTitle}Table = TableRegistry::getTableLocator()->get('$tableTitle');" .
            "\n\n" .
            "/** @var $entityTitle \$$entityTitle */\n" .
            "\$$entityTitle = \${$tableTitle}Table->get(\$id);\n\n";

        foreach ($fields as $field) {
            $template .= "\${$entityTitle}->set('$field', '');\n\n";
        }

        $template .=
            "\$validationErrors = \${$tableTitle}Table->getValidator()->errors(\${$entityTitle}->toArray(), false);\n\n".
            "if (empty(\$validationErrors)) {\n" .
            "\${$tableTitle}Table->saveOrFail(\$$entityTitle);\n" .
            "}";

        return $template;
    }
}