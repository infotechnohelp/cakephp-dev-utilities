<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\PluginTable;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;

/**
 * Class Basic
 * @package Infotechnohelp\CakeDevUtilities\PatchTemplates\Shell
 */
class CreateAndSaveEntity extends Template
{
    /**
     * @var array
     */
    protected $inputKeys = [
        'pluginTableAlias' => ['string', 'Infotechnohelp/CakeDevUtilities.TestEntities'],
        'entityFields' => ['automatic', 'Leave empty']
    ];

    /**
     * FetchAllRows constructor.
     * @param array|null $input
     */
    public function __construct(array $input = null)
    {
        parent::__construct($input);

        $preparedInput = $this->prepareInput($input);

        $this->patches = [
            $this->usages($preparedInput),
            $this->template($preparedInput),
        ];
    }

    /**
     * @param array $_
     * @return string
     */
    private function usages(array $_): string
    {
        $pluginTableAlias = $_['pluginTableAlias'];

        $tableTitle = explode('.', $pluginTableAlias)[1];

        $Table = TableRegistry::getTableLocator()->get($pluginTableAlias);

        $entityClass = $Table->getEntityClass();

        $tableClass = explode('Entity', $entityClass)[0] . "Table\\{$tableTitle}Table" ;

        return "use Cake\ORM\TableRegistry;\n" .
            "use $tableClass;\n" .
            "use $entityClass;\n";
    }

    /**
     * @param array $_
     * @return string
     */
    private function template(array $_): string
    {
        $pluginTableAlias = $_['pluginTableAlias'];

        $tableTitle = explode('.', $pluginTableAlias)[1];

        $Table = TableRegistry::getTableLocator()->get($pluginTableAlias);

        $entityClass = $Table->getEntityClass();

        $tableClass = explode('Entity', $entityClass)[0] . "Table\\{$tableTitle}Table" ;

        $fields = $this->getEntityFields($pluginTableAlias);

        $entityTitle = Inflector::singularize($tableTitle);

        $template =
            "/** @var {$tableTitle}Table \${$tableTitle}Table */\n" .
            "\${$tableTitle}Table = TableRegistry::getTableLocator()->get('$tableTitle');" .
            "\n\n" .
            "/** @var $entityTitle \$$entityTitle */\n" .
            "\$$entityTitle = \${$tableTitle}Table->newEntity([\n";

        foreach ($fields as $field) {
            $template .= "\"$field\" => \"\",\n";
        }

        $template .= "]);" .
            "\n\n" .
            "\${$tableTitle}Table->saveOrFail(\$$entityTitle);";


        return $template;
    }
}