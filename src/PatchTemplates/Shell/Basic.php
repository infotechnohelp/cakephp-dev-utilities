<?php

namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\Shell;

use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;

/**
 * Class Basic
 * @package Infotechnohelp\CakeDevUtilities\PatchTemplates\Shell
 */
class Basic extends Template
{
    /**
     * @var array
     */
    protected $inputKeys = [
        'tableTitle' => ['string', 'Articles'],
    ];

    /**
     * FetchAllRows constructor.
     * @param array|null $input
     */
    public function __construct(array $input = null)
    {
        parent::__construct($input);

        $this->patches = [
            "use Cake\ORM\TableRegistry;\n",
            self::template($this->prepareInput($input)),
        ];
    }

    /**
     * @param array $_
     * @return string
     */
    private static function template(array $_): string
    {
        $template =
            "$%sTable = TableRegistry::getTableLocator()->get('%s');" .
            "\n\n" .
            "\$result = $%sTable->find()->all();";

        return sprintf($template, $_['tableTitle'], $_['tableTitle'], $_['tableTitle']);
    }
}