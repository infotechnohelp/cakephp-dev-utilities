<?php

namespace Infotechnohelp\CakeDevUtilities\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;

/**
 * Class CreateTemplateShell
 * @package Infotechnohelp\CakeDevUtilities\Shell
 */
class CreateTemplateShell extends Shell
{
    /**
     * @param string $groupTitle
     * @param string $templateTitle
     * @return string
     */
    private function template(string $groupTitle, string $templateTitle): string
    {
        $template =
            "<?php\n\n" .
            "namespace Infotechnohelp\CakeDevUtilities\PatchTemplates\\$groupTitle;\n\n" .
            "use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;\n\n" .
            "/**\n* Class $templateTitle\n* @package Infotechnohelp\CakeDevUtilities\PatchTemplates\\$groupTitle\n*/" .
            "\n" .
            "class $templateTitle extends Template{\n" .
            "/**\n* @var array\n*/\n" .
            "protected \$inputKeys = [\n'' => ['',''],\n];\n\n" .
            "/**\n* $templateTitle constructor.\n* @param array|null \$input\n*/\n" .
            "public function __construct(array \$input = null){\n" .
            "parent::__construct(\$input);\n\n" .
            "\$preparedInput = \$this->prepareInput(\$input);\n\n" .
            "\$this->patches = [\n\$this->usages(\$preparedInput),\n\$this->template(\$preparedInput),\n];}\n\n" .
            "/**\n* @param array \$_\n* @return string\n*/\n" .
            "private function usages(array \$_): string{return \"\";}\n" .
            "/**\n* @param array \$_\n* @return string\n*/\n" .
            "private function template(array \$_): string{\$template = \"\";\n\nreturn \$template;}\n" .
            "}";
            
            
        return $template;
    }
    
    /**
     * @return bool|int|void|null
     */
    public function main(string $groupTitle, string $templateTitle)
    {
        $file = new File(dirname(__DIR__) . DS . 'PatchTemplates' . DS . $groupTitle . DS . $templateTitle . '.php', true);

        $file->write($this->template($groupTitle, $templateTitle));
    }
}