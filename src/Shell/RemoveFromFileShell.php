<?php

namespace Infotechnohelp\CakeDevUtilities\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Infotechnohelp\CakeDevUtilities\Lib\FileWizard;

/**
 * Class RemoveFromFileShell
 * @package Infotechnohelp\CakeDevUtilities\Shell
 */
class RemoveFromFileShell extends Shell
{
    /**
     * @return bool|int|void|null
     */
    public function main(string $filePath)
    {
        (new FileWizard())->removeInsideNeedlesFromFile($filePath);
    }
}