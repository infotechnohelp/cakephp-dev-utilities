<?php

namespace Infotechnohelp\CakeDevUtilities\Shell;

use Cake\Console\Shell;
use Cake\Filesystem\File;
use Cake\Utility\Inflector;
use Infotechnohelp\CakeDevUtilities\Lib\FileWizard;
use Symfony\Component\Yaml\Yaml;

/**
 * Class TestShell
 * @package Infotechnohelp\CakeDevUtilities\Shell
 */
class BakeMigrationShell extends Shell
{
    private function parseTableAlias(string $tableAlias)
    {
        $result = [
            'tableTitle' => null,
            'linking' => [
                'columns' => [],
                'tables' => [],
            ],
        ];


        if (strpos($tableAlias, '..') !== false) {
            $exploded = explode('..', $tableAlias);

            foreach (explode('+', $exploded[0]) as $linkedTableTitle) {
                $result['linking']['tables'][] = $linkedTableTitle;
            }

            if (strpos($exploded[1], '.') === false) {
                $result['tableTitle'] = $exploded[1];
                return $result;
            }

            $exploded = explode('.', $exploded[1]);

            foreach (explode('+', $exploded[0]) as $linkedTableTitle) {
                $result['linking']['columns'][] = $linkedTableTitle;
            }

            $result['tableTitle'] = $exploded[1];

            return $result;
        }

        if (strpos($tableAlias, '.') !== false) {
            $exploded = explode('.', $tableAlias);

            foreach (explode('+', $exploded[0]) as $linkedTableTitle) {
                $result['linking']['columns'][] = $linkedTableTitle;
            }

            $result['tableTitle'] = $exploded[1];

            return $result;
        }

        $result['tableTitle'] = $tableAlias;

        return $result;
    }


    private function prepareMigrationConfig(array $parsedMigrationYaml): array
    {
        $result = [];

        foreach ($parsedMigrationYaml as $tableAlias => $columns) {
            $tableResult = [
                'table' => $this->parseTableAlias($tableAlias),
                'columns' => [],
            ];

            if ($columns !== null) {
                foreach ($columns as $columnData) {

                    if ($columnData !== null) {
                        foreach ($columnData as $columnsAlias => $columnOptions) {

                            $columnTitle = $columnsAlias;
                            $columnType = null;

                            if (strpos($columnsAlias, ':') !== false) {
                                list($columnTitle, $columnType) = explode(':', $columnsAlias);
                            }

                            $tableResult['columns'][] = [
                                'title' => $columnTitle,
                                'type' => $columnType,
                                'options' => $columnOptions,
                            ];
                        }
                    }

                }
            }

            $result[] = $tableResult;
        }

        return $result;
    }


    private function prepareType(string $type)
    {
        $map = Yaml::parse(file_get_contents(PLUGIN_CONFIG . 'YamlMigrations/settings/columnTypeMap.yml'));

        return $map[$type];
    }

    private function prepareColumnOptionValue($value)
    {
        if (is_string($value)) {
            return "'$value'";
        }

        if (is_int($value)) {
            return $value;
        }

        if ($value === null) {
            return 'null';
        }

        if ($value === false) {
            return 'false';
        }

        if ($value === true) {
            return 'true';
        }

    }

    private function updateYamlArray(array $firstArray, array $secondArray = null): array
    {
        if ($secondArray !== null) {
            foreach ($secondArray as $key => $value) {
                if ($key === '_null') {
                    $key = 'null';
                }

                $firstArray[$key] = $value;
            }
        }

        return $firstArray;
    }

    private function prepareColumnOptions(string $type, array $columnOptions = null)
    {
        $defaultOptions =
            Yaml::parse(file_get_contents(PLUGIN_CONFIG .
                "YamlMigrations/settings/columnTypeDefaults/$type.yml"));

        if (array_key_exists('_null', $defaultOptions)) {
            $defaultOptions['null'] = $defaultOptions['_null'];
            unset($defaultOptions['_null']);
        }

        return $this->updateYamlArray($defaultOptions, $columnOptions);
    }

    private function renderColumnMethod(array $columnConfig)
    {
        $title = $columnConfig['title'];

        $type = $this->prepareType($columnConfig['type']);

        $result = "->addColumn('$title', '$type', [\n";

        $columnOptions = $this->prepareColumnOptions($type, $columnConfig['options']);

        if ($columnOptions !== null) {
            foreach ($columnOptions as $key => $value) {
                $value = $this->prepareColumnOptionValue($value);
                $result .= "'$key' => $value,\n";
            }
        }

        $result .= "])\n";

        if ($type !== 'text') {
            $result .= "->addIndex(['$title',])\n";
        }

        return $result;
    }

    private function renderTableMethod(array $tableConfig)
    {
        $title = $tableConfig['table']['tableTitle'];

        $columns = $tableConfig['columns'];

        $underscoredTitle = Inflector::underscore($title);

        $result = "private function $title(){\n" .
            "\$table = \$this->table('$underscoredTitle');\n" .
            "\$table\n";

        if (!empty($tableConfig['table']['linking']['columns'])) {
            foreach ($tableConfig['table']['linking']['columns'] as $linkedTable) {
                $columnTitle = Inflector::underscore(Inflector::singularize($linkedTable)) . '_id';

                $result .=
                    "->addColumn('$columnTitle', 'integer', [\n" .
                    "'default' => null,\n" .
                    "'limit' => 11,\n" .
                    "'null' => false,\n" .
                    "])\n" .
                    "->addIndex(['$columnTitle',])\n";
            }
        }

        $timestampBehaviour = true;

        foreach ($columns as $columnConfig) {
            if ($columnConfig['title'] === 'timestampBehaviour') {
                $timestampBehaviour = $columnConfig['options'];
                continue;
            }
            $result .= $this->renderColumnMethod($columnConfig);
        }

        if ($timestampBehaviour) {
            $result .=
                "->addColumn('created', 'datetime', [\n" .
                "'default' => null,\n" .
                "'limit' => null,\n" .
                "'null' => true,\n" .
                "])\n" .
                "->addIndex(['created',])\n";

            $result .=
                "->addColumn('modified', 'datetime', [\n" .
                "'default' => null,\n" .
                "'limit' => null,\n" .
                "'null' => true,\n" .
                "])\n" .
                "->addIndex(['modified',])\n";
        }

        $result .=
            "->create();\n" .
            "}\n\n";

        return $result;
    }

    private function renderLinkingTableMethods(array $tableConfig, array &$linkingTables)
    {
        $title = $tableConfig['table']['tableTitle'];
        $linkedTables = $tableConfig['table']['linking']['tables'];

        $columns = $tableConfig['columns'];

        $result = '';

        foreach ($linkedTables as $linkedTable) {
            $tableTitles = [$title, $linkedTable];
            sort($tableTitles);

            $tableTitle = implode('', $tableTitles);

            $linkingTables[] = $tableTitle;

            $underscoredTitle = Inflector::underscore($tableTitle);

            $result .= "private function $tableTitle(){\n" .
                "\$table = \$this->table('$underscoredTitle');\n" .
                "\$table\n";

            foreach ($tableTitles as $tableTitle) {
                $columnTitle = Inflector::underscore(Inflector::singularize($tableTitle)) . '_id';

                $result .=
                    "->addColumn('$columnTitle', 'integer', [\n" .
                    "'default' => null,\n" .
                    "'limit' => 11,\n" .
                    "'null' => false,\n" .
                    "])\n" .
                    "->addIndex(['$columnTitle',])\n";
            }

            $result .=
                "->addColumn('created', 'datetime', [\n" .
                "'default' => null,\n" .
                "'limit' => null,\n" .
                "'null' => true,\n" .
                "])\n" .
                "->addIndex(['created',])\n";

            $result .=
                "->addColumn('modified', 'datetime', [\n" .
                "'default' => null,\n" .
                "'limit' => null,\n" .
                "'null' => true,\n" .
                "])\n" .
                "->addIndex(['modified',])\n";

            $result .=
                "->create();\n" .
                "}\n\n";
        }


        return $result;
    }

    /**
     * @return bool|int|void|null
     */
    public function main(
        string $migrationTitle = 'Initialize',
        string $yamlTitle = 'migrations',
        string $outputDir = 'config/Migrations/',
        string $yamlDirPath = PLUGIN_CONFIG . 'YamlMigrations' . DS
    )
    {
        $linkingTables = [];

        $config = Yaml::parse(file_get_contents($yamlDirPath . "$yamlTitle.yml"));

        $migrationConfig = ($this->prepareMigrationConfig($config));

        $result =
            "<?php\n\n" .
            "use Migrations\\AbstractMigration;\n\n" .
            "class $migrationTitle extends AbstractMigration{\n";

        foreach ($migrationConfig as $tableConfig) {
            $result .= $this->renderTableMethod($tableConfig);

            if (!empty($tableConfig['table']['linking']['tables'])) {
                $result .= $this->renderLinkingTableMethods($tableConfig, $linkingTables);
            }
        }


        $result .= "public function up(){\n";

        foreach ($migrationConfig as $tableConfig) {
            $title = $tableConfig['table']['tableTitle'];
            $result .= "\$this->$title();\n";
        }

        foreach ($linkingTables as $linkingTable) {
            $title = $tableConfig['table']['tableTitle'];
            $result .= "\$this->$linkingTable();\n";
        }

        $result .= "}\n\n";

        $result .= "public function down(){\n";

        foreach ($migrationConfig as $tableConfig) {
            $underscoredTableTitle = Inflector::underscore($tableConfig['table']['tableTitle']);
            $result .= "\$this->table('$underscoredTableTitle')->drop()->update();\n";
        }

        foreach ($linkingTables as $linkingTable) {
            $underscoredTableTitle = Inflector::underscore($linkingTable);
            $result .= "\$this->table('$underscoredTableTitle')->drop()->update();\n";
        }

        $result .= "}\n\n}";

        $migrationFileTitle = (new \DateTime())->format('YmdHis') . '_' . $migrationTitle . '.php';

        $file = new File($outputDir . $migrationFileTitle);
        $file->write($result);

        $this->out($outputDir . $migrationFileTitle);
    }
}