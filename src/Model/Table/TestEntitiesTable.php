<?php
namespace Infotechnohelp\CakeDevUtilities\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TestEntities Model
 *
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity get($primaryKey, $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity newEntity($data = null, array $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity[] newEntities(array $data, array $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity[] patchEntities($entities, array $data, array $options = [])
 * @method \Infotechnohelp\CakeDevUtilities\Model\Entity\TestEntity findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TestEntitiesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('test_entities');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->allowEmptyString('title', false);

        $validator
            ->integer('age')
            ->requirePresence('age', 'create')
            ->allowEmptyString('age', false);

        return $validator;
    }
}
