<?php

namespace Infotechnohelp\CakeDevUtilities\Lib;

use Symfony\Component\Yaml\Yaml;

/**
 * Class FileWizardManager
 * @package Infotechnohelp\CakeDevUtilities\Lib
 */
class FileWizardManager extends FileWizard
{
    /**
     * @param string $filePath
     * @param string $templateClass
     * @param array|null $input
     */
    public function patch(string $filePath, string $templateClass, array $input = null, array $ignoredPatches = []): bool
    {
        $template = new $templateClass($input);

        $config = Yaml::parse(file_get_contents(PLUGIN_CONFIG . 'config.yml'));

        $needle = $config['needle'] ?? '#';

        $this->replaceNeedlesInFile($filePath, $template, $ignoredPatches, $needle);
        
        return true;
    }
}