<?php

namespace Infotechnohelp\CakeDevUtilities\Lib;

use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;
use Cake\Filesystem\File;
use Symfony\Component\Yaml\Yaml;

/**
 * Class FileWizard
 * @package Infotechnohelp\CakeDevUtilities\Lib
 */
class FileWizard
{
    /**
     * @param string $stack
     * @param array $patches
     * @param int $patchAmount
     * @param string $needle
     * @return string
     * @throws \Exception
     */
    private function replaceNeedles(string $stack, array $patches, int $patchAmount = 1, array $ignored = [], string $needle = '#'): string
    {
        if (substr_count($stack, $needle) === 0) {
            throw new \Exception("No needle '$needle' found in:\n$stack");
        }

        if (substr_count($stack, $needle) !== $patchAmount - count($ignored)) {
            throw new \Exception('Needle amount does not match');
        }

        $result = $stack;

        $i = 0;

        foreach ($patches as $patch) {
            if (in_array($i, $ignored, true)) {
                $i++;
                continue;
            }

            $pos = strpos($result, $needle);
            if ($pos !== false) {
                $result = substr_replace($result, $patch, $pos, strlen($needle));
            }

            $i++;
        }


        return $result;
    }

    /**
     * @param string $path
     * @param Template $template
     * @param string $needle
     * @throws \Exception
     */
    protected function replaceNeedlesInFile(string $path, Template $template, array $ignored = [], string $needle = '#')
    {
        $file = new File($path);

        $file->write($this->replaceNeedles($file->read(), $template->getPatches(), $template->getPatchAmount(), $ignored, $needle));
    }

    /**
     * @param string $path
     */
    public function removeInsideNeedlesFromFile(string $path)
    {
        $config = Yaml::parse(file_get_contents(PLUGIN_CONFIG . 'config.yml'));

        $needle = $config['needle'] ?? '#';

        $file = new File($path);

        $lastPos = 0;

        while (($lastPos = strpos($file->read(), $needle, $lastPos)) !== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }

        if (count($positions) !== 2) {
            throw new \Exception("Needle '$needle' amount is not 2");
        }

        $file->write(substr_replace($file->read(), '', $positions[0], $positions[1] - $positions[0] + 1));
    }

}