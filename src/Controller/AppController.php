<?php

declare(strict_types=1);

namespace Infotechnohelp\CakeDevUtilities\Controller;

use \Cake\Controller\Controller;

/**
 * Class AppController
 * @package Infotechnohelp\CakeDevUtilities\Controller
 */
class AppController extends Controller
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
}
