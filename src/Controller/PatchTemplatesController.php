<?php

declare(strict_types=1);

namespace Infotechnohelp\CakeDevUtilities\Controller;

use \Cake\Controller\Controller;
use Cake\Filesystem\Folder;
use Infotechnohelp\CakeDevUtilities\Lib\FileWizardManager;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Table\FetchAllRows;
use Infotechnohelp\CakeDevUtilities\PatchTemplates\Template;
use Symfony\Component\Yaml\Yaml;

/**
 * Class TemplatesController
 * @package Infotechnohelp\CakeDevUtilities\Controller
 */
class PatchTemplatesController extends Controller
{

    public function index()
    {
        $this->viewBuilder()->setLayout(false);

        $folder = new Folder(dirname(__DIR__) . DS . 'PatchTemplates');

        $result = [];

        foreach ($folder->subdirectories() as $subdirectory) {

            foreach ((new Folder($subdirectory))->read()[1] as $templateFileTitle) {
                $templateGroup = basename($subdirectory);
                $templateClass = pathinfo($templateFileTitle)['filename'];
                $classTitle = "Infotechnohelp\CakeDevUtilities\PatchTemplates\\$templateGroup\\$templateClass";

                /** @var Template $Template */
                $Template = new $classTitle();

                $result[$templateGroup][] = [
                    'class' => $classTitle,
                    'input' => $Template->getInputKeys(),
                    'patches' => $Template->getPatches(),
                ];
            }

        }

        $this->set('data', $result);


        $result = [];

        $folder = new Folder(APP . 'Model' . DS . 'Table');

        foreach ($folder->read()[1] as $tableClassTitle) {
            $result[] = explode('Table.php', $tableClassTitle)[0];
        }

        $this->set('tables', $result);
        

        $this->set('pluginTableAliases',
            Yaml::parse(file_get_contents(PLUGIN_CONFIG . 'pluginTableAliases.yml')));
        

        $this->set('requestData', $this->getRequest()->getSession()->read('patch.requestData'));

        $this->getRequest()->getSession()->destroy();
    }

    /**
     * @return \Cake\Http\Response|null
     */
    public function patch()
    {
        $this->autoRender = false;

        $data = $this->getRequest()->getData();

        $result = (new FileWizardManager())->patch(
            $data['filePath'], $data['templateClass'],
            json_decode($data['inputJson'], true),
            json_decode($data['ignoredPatches'], true)
        );

        if ($result) {
            $this->getRequest()->getSession()->write('patch.requestData', $data);

            return $this->redirect($this->referer() . "#" . $data['templateClass']);
        }
    }
}
