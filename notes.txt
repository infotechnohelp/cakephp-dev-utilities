Transactional - Lib/Liability/Manager 207

Full ORM QUERY

first()/last()/firstOrFail()

Contain() - Lib/Customer/Customer.php 24, Lib/DocumentManager/Profit/ProfitCreator.php 92

matching() -

notMatching()

filter()

or_()

order()

where()

andWhere() / orWhere()

where() + andWhere() - Lib/Liability/Manager 239

enableBufferedResults(false)

select()

where(['Liabilities.id IN' => $liabilities])

where(['TemplateLiabiltiies.last_active_date IS NULL'])

where(['Liabilities.formation_date >' => $startDate->format('Y-m-d')]) // <, >=, <=

https://stackoverflow.com/questions/43656590/what-order-are-andwhere-orwhere-where-methods-evaluated-in-cakephp-3


  $query = $this->Orders->find()
        ->where(['author_id' => 2])
        ->orWhere(['author_id' => 3])
        ->andWhere([
            'OR'=>[
                ['promoted' => true],
                ['published' => true,
                    'view_count >' => 10]
            ]
        ]);


            $query = $this->Orders->find()
                    ->where(['author_id' => 2])
                    ->orWhere(['author_id' => 3])
                    ->andWhere(function (QueryExpression $exp) {
                        return $exp->or_([
                            'promoted' => true,
                            ['published' => true,
                                'view_count >' => 10]
                        ]);
                    })->toArray();