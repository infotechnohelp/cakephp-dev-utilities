<?php
namespace Infotechnohelp\CakeDevUtilities\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Infotechnohelp\CakeDevUtilities\Model\Table\TestEntitiesTable;

/**
 * Infotechnohelp\CakeDevUtilities\Model\Table\TestEntitiesTable Test Case
 */
class TestEntitiesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Infotechnohelp\CakeDevUtilities\Model\Table\TestEntitiesTable
     */
    public $TestEntities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Infotechnohelp/CakeDevUtilities.TestEntities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TestEntities') ? [] : ['className' => TestEntitiesTable::class];
        $this->TestEntities = TableRegistry::getTableLocator()->get('TestEntities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TestEntities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
